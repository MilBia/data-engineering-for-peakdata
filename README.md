# Data Engineering for PeakData
___

## How to build and run
___
```
python3.9 -m venv env
source env/bin/activate
pip install -r requirements.txt
python main.py publications_min.csv.gz 0.8 4
```

* input values are:
  * path_to_input_file
  * in watch percent words should be similar to be considered as duplicates ( 0-1) 
  * number of processes to parallel searching for duplicates 

## Documentation
___
Function `str_authors_to_lists` use `try` instead of `if`, because handling of exception can be faster, 
then checking the condition. From quick check on data, it was easy to reuse `nan` from the row in case 
`TypeError`. Using regex to make that parsing more efficient.

Function `are_initials` checking is any if them is initial and both have the same first letter.

Function `are_similar` checking is two words are similar, using Levenshtein package.

Function `is_duplicate` checking is two literals with the sam number of elements are similar 

Function `find_unique_name` checking if author have potential duplicate in iterable and return most probable 
cimbination)

Function `find_unique_fullname` checking if author have potential duplicate in iterable and return most probable 
cimbination) 

Function `get_unique_names` find list of unique names in gaven series


`data.apply(str_authors_to_lists, axis=1)` - will result with a `pd.Series` of data, 
but it not harms performance, so why not   

`authors.dropna().explode()` - remove all rows without authors, which could go true `str_authors_to_lists`
and convert list od authors from each row to separate rows.  

```
    authors = authors.str.replace(r"\.|-", " ", regex=True)
    authors = authors.str.rsplit()
    authors = authors.str.join(" ")
```
remove unwanted chars from names.

```
    authors = authors.drop_duplicates()
    authors = authors.str.rsplit()
    authors.reset_index(drop=True, inplace=True)
```
remove easy to find duplicates,go back to list representation and reindex of Series.

`pd.DataFrame({"Author": unique_names})` - convert data back to `pd.DataFrame` to easier save it

`authors.to_csv('unique_people.csv', index=False)` - save results to file with name as gaven in example and 
without indexes also corresponding to example 

I use as many package functions to ensure fast computation. 

## Potential failure points and bottlenecks
___
- parsing the `"authors"` column: new characters to excuse, other not excepted errors
- process.Pool memory leak
- Levenshtein could turn out to be to slow or mismatched to the problem

## Remaining steps needed before putting deploying code to a production system
___
- split `main()` to functions with loading and extracting data, and removing duplicates in them 
- add more parametrisation, specially for column naming, but also input nad output paths
- check code with `flake8` or `black`
- prepare some tests
- integrate to existing approaches, whatever it requires, like creating an object or endpoint

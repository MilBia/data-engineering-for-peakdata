import re
import sys
from functools import partial
from multiprocessing import Pool, cpu_count
from typing import List, Union

import Levenshtein
import pandas as pd

MINIMAL_DUPLICATES_SIMILARITY = 0.8
PROCESSES_NUMBER = 1


def str_authors_to_lists(row: pd.Series) -> Union[List, None]:
    authors = row["authors"]
    try:
        authors = re.sub(r"[\[\]\'\"]", "", authors)
    except TypeError:
        return None
    return authors.split(",")


def are_initials(str1: str, str2: str) -> bool:
    return (len(str1) == 1 or len(str2) == 1) and str1[0].lower() == str2[0].lower()


def are_similar(author1_name_part: str, author2_name_part: str) -> bool:
    return Levenshtein.ratio(author1_name_part.lower(), author2_name_part.lower()) >= MINIMAL_SIMILARITY


def is_duplicate(author1_name: List, author2_name: List) -> bool:
    return all(map(lambda a1, a2: are_initials(a1, a2) or are_similar(a1, a2), author1_name, author2_name))


def find_unique_name(author: List, tmp_authors: pd.Series, tmp_authors_str: pd.Series) -> str:
    is_really_duplicate = partial(is_duplicate, author2_name=author)
    mask = list(map(is_really_duplicate, tmp_authors))
    potential_duplicates = tmp_authors[mask]
    potential_duplicates = potential_duplicates.tolist()
    if len(potential_duplicates) > 1 and any(any(len(part) == 1 for part in name) for name in potential_duplicates):
        final_name = []
        for i in range(len(potential_duplicates[0])):
            if any(len(duplicate[i]) == 1 for duplicate in potential_duplicates):
                final_name.append(max(potential_duplicates, key=(lambda duplicate: len(duplicate[i])))[i])
            else:
                final_name.append(potential_duplicates[-1][i])
        author = final_name
    else:
        author = potential_duplicates[0]
    author = " ".join(author)
    is_really_similar = partial(are_similar, author2_name_part=author)
    mask = list(map(is_really_similar, tmp_authors_str))
    potential_duplicates = tmp_authors_str[mask]
    if potential_duplicates.empty:
        return author
    return max(potential_duplicates, key=(lambda duplicate: len(duplicate)))


def find_unique_fullname(author: str, tmp_authors: List) -> str:
    is_really_similar = partial(are_similar, author2_name_part=author)
    potential_duplicates = list(filter(is_really_similar, tmp_authors))
    if not len(potential_duplicates):
        return author
    return max(potential_duplicates, key=(lambda duplicate: len(duplicate)))


def get_unique_names(authors: pd.Series, pool_processes: int) -> List:
    true_names = []
    name_parts = 1
    while not authors.empty:
        tmp_authors = authors[authors.str.len() == name_parts]
        authors.drop(index=tmp_authors.index, inplace=True)

        if not tmp_authors.empty:
            is_really_duplicate = partial(
                find_unique_name,
                tmp_authors=tmp_authors,
                tmp_authors_str=tmp_authors.str.join(" ")
            )
            with Pool(pool_processes) as p:
                true_names += set(p.map(is_really_duplicate, tmp_authors))

        name_parts += 1

    is_really_unique = partial(find_unique_fullname, tmp_authors=true_names)
    with Pool(pool_processes) as p:
        true_names = set(p.map(is_really_unique, true_names))

    return sorted(true_names)


def main(input_file_path: str, pool_processes: int) -> None:
    # load authors
    data = pd.read_csv(input_file_path, compression="gzip")
    authors = data.apply(str_authors_to_lists, axis=1)
    # extract elements of name
    authors = authors.dropna().explode()
    authors = authors.str.replace(r"\.|-", " ", regex=True)
    authors = authors.str.rsplit()
    authors = authors.str.join(" ")
    # remove duplicates
    authors = authors.drop_duplicates()
    authors = authors.str.rsplit()
    authors.reset_index(drop=True, inplace=True)
    unique_names = get_unique_names(authors, pool_processes)
    authors = pd.DataFrame({"Author": unique_names})
    # save results to file
    authors.to_csv("unique_people.csv", index=False)


if __name__ == '__main__':
    if len(sys.argv[1]) > 1:
        file_path = sys.argv[1]
        try:
            minimal_duplicates_similarity = float(sys.argv[2])
            if minimal_duplicates_similarity > 1:
                minimal_duplicates_similarity = 1
        except (IndexError, ValueError):
            minimal_duplicates_similarity = MINIMAL_DUPLICATES_SIMILARITY
        try:
            processes = int(sys.argv[3])
            if processes >= (max_processes := cpu_count()):
                processes = max_processes
        except (IndexError, ValueError):
            processes = PROCESSES_NUMBER
        global MINIMAL_SIMILARITY
        MINIMAL_SIMILARITY = minimal_duplicates_similarity
        main(file_path, processes)
    else:
        "Please pass path to data file on running that file"
